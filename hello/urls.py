from django.conf.urls import include, url
from django.urls import path

from .views import *

app_name = 'hello'

urlpatterns = [
    # index view
    path('', purchase, name='purchase'),

    # user auth views
    path('signin/', signin, name='signin'),

    # sections views
    path('section1/', section1, name='section1'),
    path('section2/', section2, name='section2'),
    path('section2/finished/', section2_finished, name='sections2_finished'),

    # sections stepts views
    path('section1/step2', section1_step2, name='section1_step2'),
    path('section2/step1', section2_step1, name='section2_step1'),
    path('section2/step3', section2_step3, name='section2_step3'),
    path('section2/overview', section2_overview, name='section2_overview'),

    # backend test urls
    path('backend_test/', backendTest.as_view(), name='backend_test'),
]
