from django.conf.urls import include, url
from django.urls import path

from django.contrib import admin
admin.autodiscover()

import hello

# Examples:
# url(r'^$', 'gettingstarted.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),

urlpatterns = [
    path('', include('hello.urls', namespace='index')),
    url(r'^db', hello.views.db, name='db'),
    path('admin/', admin.site.urls),
]
