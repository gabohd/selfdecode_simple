import requests
import math

from scipy.stats import norm
from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from .models import Greeting

# Create your views here.
def purchase(request):
    # return HttpResponse('Hello from Python!')
    #return render(request, 'index.html')
    #r = requests.get('http://httpbin.org/status/418')
    #print(r.text)
    #return HttpResponse('<pre>' + r.text + '</pre>')
    return render(request, 'purchase.html')


# Simple templates views
def signin(request):
    return render(request, 'signin.html')


def section1(request):
    return render(request, 'section_1.html')


def section1_step2(request):
    return render(request, 'section_1_step_2.html')


def section2(request):
    return render(request, 'section_2.html')


def section2_step1(request):
    return render(request, 'section_2_step_1.html')


def section2_step3(request):
    return render(request, 'section_2_step_3.html')

def section2_overview(request):
    return render(request, 'section_2_overview.html')


def section2_finished(request):
    return render(request, 'section_2_finished.html')


# Backend test view for A/B median test
@method_decorator(csrf_exempt, name='dispatch')
class backendTest(View):
    def get(self, request):
        return render(request, 'backend_test.html')

    def post(self, request):
        # Minimum value for fields
        min_n = 15.0
        # confidence = float(1.96) 95% confidence
        confidence = 1.65 # 90% confidence like the page

        if (not request.POST['control_visitors'] or not request.POST['control_conversions']
            or not request.POST['variation_visitors'] or not request.POST['variation_conversions']):
            return JsonResponse({'msg': 'Please enter numbers in all control and variation fields.'}, status=400)
        else:
            # Control and Variation initialization:
            control_visitors = float(request.POST['control_visitors'])
            control_conv = float(request.POST['control_conversions'])
            variation_visitors = float(request.POST['variation_visitors'])
            variation_conv = float(request.POST['variation_conversions'])

            if control_visitors < min_n:
                return JsonResponse({'msg': 'There must be at least 15 control trials for this tool to produce any results.'}, status=400)
            elif variation_visitors < min_n:
                return JsonResponse({'msg': 'There must be at least 15 variation trials for this tool to produce any results.'}, status=400)
            elif variation_visitors < 0 or control_visitors < 0 or variation_conv < 0 or control_conv < 0:
                return JsonResponse({'msg': 'Please just positive numbers.'}, status=400)
            else:
                p_value = 0
                # Control calculations
                control_p = control_conv / control_visitors
                control_se = float(math.sqrt(control_p * (1-control_p) / control_visitors))

                if(control_p - confidence*control_se < 0):
                    cont_conv_min_lim = 0
                else:
                    cont_conv_min_lim = control_p-confidence*control_se

                if(control_p + confidence*control_se > 1):
                    cont_conv_max_lim = 1
                else:
                    cont_conv_max_lim = control_p+confidence*control_se

                # Variation calculations
                variation_p = variation_conv / variation_visitors
                variation_se = float(math.sqrt(variation_p * (1-variation_p) / variation_visitors))

                if(variation_p-confidence*variation_se < 0):
                    var_conv_min_lim = 0
                else:
                    var_conv_min_lim = variation_p-confidence*variation_se

                if(variation_p+confidence*variation_se > 1):
                    var_conv_max_lim = 1
                else:
                    var_conv_max_lim =  variation_p+confidence*variation_se

                # Z-score
                z_score = (control_p - variation_p) / float(math.sqrt(math.pow(control_se, 2) + math.pow(variation_se, 2)))
                
                # Normal Distribution
                p_value = float(norm.cdf(z_score, 0, 1))

                return JsonResponse({'p_value': p_value})


def db(request):

    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, 'db.html', {'greetings': greetings})

